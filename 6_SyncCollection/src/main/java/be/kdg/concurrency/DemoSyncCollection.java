package be.kdg.concurrency;

import java.util.*;

/**
 * https://www.baeldung.com/java-synchronized-collections
 */
public class DemoSyncCollection {
    public static void main(String[] args) throws InterruptedException {
        List<Integer> syncList = new ArrayList<>();
        Runnable listOperations = () -> {
            syncList.addAll(Arrays.asList(1, 2, 3, 4, 5, 6));
        };

        Thread thread1 = new Thread(listOperations);
        Thread thread2 = new Thread(listOperations);
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();

        System.out.println("Inhoud van syncList: " + syncList);

    }
}
