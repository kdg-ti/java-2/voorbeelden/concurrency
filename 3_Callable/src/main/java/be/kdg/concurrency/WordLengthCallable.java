package be.kdg.concurrency;

import java.util.concurrent.Callable;

public class WordLengthCallable implements Callable<Integer> {
    private String word;

    public WordLengthCallable(String word) {
        this.word = word;
    }

    public Integer call() throws Exception {
        Thread.sleep(2000);
        System.out.print(word.length() + "... ");
        return word.length();
    }
}
