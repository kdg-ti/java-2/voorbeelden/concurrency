package be.kdg.concurrency;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CallableDemo {
    public static void main(String[] args) throws Exception {
        String str = "That's one small step for man, one giant leap for mankind";
        String[] words = str.split(" ");

        System.out.println("Original: " + str);
        ExecutorService pool = Executors.newFixedThreadPool(5);
        List<Future<Integer>> futureLengths = new ArrayList<>();
        for (String word : words) {
            Callable<Integer> callable = new WordLengthCallable(word);
            Future<Integer> future = pool.submit(callable);
            futureLengths.add(future);
        }
        int sum = 0;
        for (Future<Integer> future : futureLengths) {
            sum += future.get();
        }
        System.out.printf("The sum of lengths is %s\n", sum);

        //TODO: Breid de code uit: herbruik de pool. Maak een nieuwe lambda-Callable die de omgekeerde string teruggeeft.


        pool.shutdown();

    }
}

/*
 Original: That's one small step for man, one giant leap for mankind
 The sum of lengths is 47
 Reversed: s'tahT eno llams pets rof ,nam eno tnaig pael rof dniknam
 */
