package be.kdg.concurrency;

import java.util.Arrays;

public class ParallelTest {
    public static void main(String[] args) {
        double[] numbers = new double[10_000_000];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = (double) i;
        }

        long start = System.nanoTime();
        Arrays.stream(numbers)
                .parallel()
                .map(nr -> nr % 2 == 0 ? Math.sqrt(nr) : Math.cbrt(nr))
                .toArray();
        long end = System.nanoTime();

        int cores = Runtime.getRuntime().availableProcessors();
        System.out.println("Level of parallelism: " + cores);
        System.out.printf("Elapsed time: %.3f ms\n", (end - start) / 1_000_000.0);
    }
}


