package concurrency.RecursiveTask;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;

public class ForkJoinTest {
    public static void main(String[] args) {
        int cores = 4;
        int treshold = 1000;

        ForkJoinPool pool = new ForkJoinPool(cores);
        double[] numbers = new double[10_000_000];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = (double) i;
        }

        try {
            Accumulate forkTask = new Accumulate(numbers, 0, numbers.length, treshold);
            long start = System.nanoTime();
            pool.invoke(forkTask);
            long end = System.nanoTime();

            System.out.println("\nVia ForkJoinTask:");
            System.out.printf("Som: %,d\n", forkTask.get());
            System.out.println("Level of parallelism: " + cores);
            System.out.printf("Elapsed time: %.3f ms\n", (end - start) / 1_000_000.0);

            Accumulate seqTask = new Accumulate(numbers, 0, numbers.length, treshold);
            start = System.nanoTime();
            long som = seqTask.sequentialCompute();
            end = System.nanoTime();

            System.out.println("\nVia gewone sequentiele verwerking:");
            System.out.printf("Som: %,d\n", som);
            System.out.printf("Elapsed time for sequential loop: %.3f ms\n", (end - start) / 1_000_000.0);

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}


