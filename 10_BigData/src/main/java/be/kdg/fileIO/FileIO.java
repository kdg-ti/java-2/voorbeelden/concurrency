package be.kdg.fileIO;

import be.kdg.model.Actor;
import be.kdg.model.Movie;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public class FileIO {
    public static Set<Movie> readMovieFile() throws IOException {
        Set<Movie> movieSet = new HashSet<>();

        Stream<String> lines =
                Files.lines(
                        Paths.get("files/movieData.txt"),
                        Charset.forName("windows-1252")
                );

        lines.forEach(
                (String line) -> {
                    String[] elements = line.split("/");
                    String title = elements[0].substring(0, elements[0].lastIndexOf("(")).trim();
                    String releaseYear = elements[0].substring(elements[0].lastIndexOf("(") + 1, elements[0].lastIndexOf(")"));

                    if (!releaseYear.contains(",")) {
                        Movie movie = new Movie(title, Integer.valueOf(releaseYear));

                        for (int i = 1; i < elements.length; i++) {
                            String[] name = elements[i].split(", ");
                            String lastName = name[0].trim();
                            String firstName = "";
                            if (name.length > 1) {
                                firstName = name[1].trim();
                            }

                            Actor actor = new Actor(lastName, firstName);
                            movie.addActor(actor);
                        }

                        movieSet.add(movie);
                    }
                }
        );
        return movieSet;
    }
}
