package be.kdg.streaming;

import be.kdg.fileIO.FileIO;
import be.kdg.model.Actor;
import be.kdg.model.Movie;

import java.util.*;
import java.util.stream.Collectors;

public class MovieStreamDemo {
    public static void main(String... args) throws Exception {
        Set<Movie> movieSet = FileIO.readMovieFile();
        System.out.println("Aantal films: " + movieSet.size());

//TODO: Zet alle acteurs in een Set en druk het aantal af.
        Set<Actor> actors = null;

        //System.out.println("Aantal acteurs: " + actors.size());

//TODO: Verzamel alle films in een map, gegroepeerd per jaar
        Map<Integer, List<Movie>> movieMap = null;

        //System.out.println("Aantal films van 1990: " + movieMap.get(1990).size());

//TODO: Zoek de film met de meeste acteurs
        Movie movieMostActors = null;

        //System.out.printf("Film met meeste acteurs (%d):\n%s\n", movieMostActors.getActors().size(), movieMostActors);

//TODO: Verzamel de filmtitels waarin Jodie Foster meespeelde in een string, gescheiden door " - "
        String filmsJodieString = "";

        System.out.println("\nFilms met Jodie Foster: " + filmsJodieString);

//TODO: Zoek van alle acteurs van wie de achternaam met "B" begint het aantal films waarin ze meespelen. Verzamel in een map!
        long start = System.nanoTime();
        Map<String, Long> actorsBcountMap = null;

//TODO Meet de tijd en vergelijk de prestaties sequentieel <> parallel
        long end = System.nanoTime();
        //System.out.println("Aantal acteurs van wie naam met 'B' begint: " + actorsBcountMap.size());
        //actorsBcountMap.forEach((k, v) -> System.out.printf("%s : %d\n", k, v));

        System.out.printf("Elapsed time: %d ms\n", (end - start) / 1000000);
    }
}


/*
 * OUTPUT:
 * Aantal films: 13891
 * Aantal acteurs: 168727
 * Aantal films van 1990: 515
 * Film met meeste acteurs (238):
 * Movie{Title=Malcolm X, ReleaseYear=1992, Actors=[Pentangelo Joe, La Marre Jean Claude, Duppin Andy, Seneca Joe, Foster Frances, Parks John, Festa John, Parker Leonard, Herlihy Ed, Ottavino John, Dowell Raye, Rubin Christopher, Nocerino Anthony, Gantt Leland, Hutchinson Tim, Harris Matthew, Cowan Colleen, Pittman Reggie, Purviance Douglas, Imperioli Michael, McDaniel James, Counts Chela, Little Thomas Iris, Mandela Nelson, Benton Vanessa, Dockery Leslie, Blackshear Aaron, Cooper Sr. Ralph, Charbonneau Jay, Clanton Natalie, Hijrah Abdul Hakeem, Mahon Michael C., Jackman Kent, Ross Monty, Barth Bruce David, Muhammad Zaahir, Ralph Michael, O'Neil Shaun, Dillon Matt, Fontenette Cytia, Plummer Christopher, Bigelow LaToyah, Dunn Peter, Richardson LaTanya, Johnson Rion, Montgomery Reggie, Reilly David, Bargeman Gregory, Jackson Janet, Richard Judine Hawkins, Jones Walter, Rock Kevin, Atwell Benjamin, Clanton Rony, McGruder Jasper, Randle Theresa, Hyman Fracaswell, Sharpton Al, Dixon Sam, Rushing Larry, S , Robinson Scot Anthony, Adu Robinson Frank, Allen Sonny, Shellman Eddie, Bascomb Rudi, Lee Spike, Brooks Yvette, Cosby Bill, Reivers David, Cudney Cliff, McCoy Larry, Humes Dyan, Johnson Magic, Jason Robert, Donovan Martin, Kelleher Tim, Givens Jessica, Guidall George, Gibson Laurie Ann, Brown Graham, Porter Fia, Lemelle Daniel, Kelly Brendan, Hewitt Jery, Blair Andre, Martin Greta, Mitchell Billy J., Cook Addison, Jones Jake-Ann, Miller Norma, Jackson Tamaraleah, Owens Richard, MacDonald James, Davis Eddie, Ibrahim El Tahara, Layman Terry, Padick Lauren, Payne Eric, Hampton Dawn, Morgan Frances, Brooks Sharon, Harris Raymond, Hubbard Dana, Rafferty George, Sayles John, Muglia Nick, Fowler III Robert H., Chandler Damon, Do-Ley Simon, Guyton Jr. Cleave, Gaines Sonny Jim, Robinson Eartha, Rogers Ken Leigh, Brazel Gerald, Pemberton Lenore, James Stephen, Duke O.L., Kelly David Patrick, Licciardello Elmer, Bryant Lance, Pierce Wendell, Poland Greg, Lindo Delroy, Robinson Michelle, Hewitt Don, Kunstler William, Quiles Ian, Kirk Renton, Freeman Jr. Al, Ferguson Sharon, Gaton Clack, Gross Mark, Easley Byron, Hollis Tommy, Griesemer John, Harcum Monique, King Wendy, Lauper Marcus, Allen Karen, Mazar Debi, Duffy Karen, Gibbs Kevan, Michaels Karen, Peck Jonathan, McKee Lonette, French Arthur, Fletcher Danielle, Gordon Rich, Ruge George Marshall, Farber Jodie, Broadnax Dwayne Cook, Fitos Joe, Rickman Patrick, Elejaide John, Schiff Richard, Berman David, Dacosta Christian J., Blanchard Terence, Burr Cheryl, Schultz Armand, Jones Judd, Hodges Terry, Mitchell Aleta, Hall Albert, Fludd David, Fichtner William, Howze Zakee, Neal Rome, Anagnos Bill, Reed Steve, Farley Mike, Seale Bobby, Francois Ryan, Simon Rogers, Odom George T., Lewis Keith, D'Onofrio Vincent, Cherry Larry M., Means Randy, Murtaugh James, Hodge Mike, Naylor Marcus, Cintron Monique, Sanabria Eddie, Bass Marlaine, Guess Michael, Esposito Giancarlo, Picart Delilah, Corley Annie, Najeeullah Mansoor, Mclaughlin Jack P., Moscaritola Vincent, Catus Gary L., Hardeman Jerome Jamal, James Lawrence, Davis Ossie, Counts Chelsea, Robinson Traci, Gordon Ricky, Green Jauquette, Cullen Michael, Reidy John, Dubose Columbia, Drayton Cisco, Folkes-LeMelle Neisha, Randazzo Steven, Sinovoi Maxwell, Aronson Steve, Monson Lex, Cooper Chuck, Goldberg Bill, Medina Hazel, Farley Teresa Yvon, Ford Clebert, Graham Dion, Attile Larry, Howard Miki, Ellis Gina, Cox Gerica, Marsh Bernard, Bassett Angela, Boyle Peter, Hanan Stephen, Kilson William E., Jordan Michael, Gilmore Phillip, Miles George Lee, Neal Elise, Barnwell Nicholas, MacKay Lizbeth, Alice Mary, El Razzac Abdul Salaam, Elkins Debra, Phillips Marc, Dewitt Anthony, Parks Muhammad, Mantz Delphine T.]}
 * <p>
 * Films met Jodie Foster: Accused, The - Anna and the King - Bugsy Malone - Candleshoe - Carny - Catchfire - Contact - Dangerous Lives of Altar Boys, The - Echoes of a Summer - Five Corners - Foxes - Freaky Friday - Hotel New Hampshire, The - Kansas City Bomber - Little Girl Who Lives Down the Lane, The - Little Man Tate - Maverick - Mesmerized - Napoleon and Samantha - Nell - One Little Indian - Panic Room - Shadows and Fog - Siesta - Silence of the Lambs, The - Sommersby - Stealing Home - Taxi Driver
 * Aantal acteurs van wie naam met 'B' begint: 15303
 * Elapsed time: 10026 ms
 */
