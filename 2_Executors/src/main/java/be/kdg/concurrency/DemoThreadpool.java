package be.kdg.concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DemoThreadpool {

	public static void main(String[] args) {
		String[] messages = "The love of money is the root of all evil".toUpperCase().split(" ");
		ExecutorService executor = Executors.newFixedThreadPool(5);
		for (String message : messages) {
			Runnable worker = new WorkerThread(message);
			executor.execute(worker);
		}

		executor.shutdown();
		while (!executor.isTerminated()) {
//            System.out.println("Main thread waits for worker threads to finish...");
//            try {
//                Thread.sleep(100);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
		}

		System.out.println("Finished all threads");
	}
}
