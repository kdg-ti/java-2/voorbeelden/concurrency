package be.kdg.concurrency;

public class WorkerThread implements Runnable {

    private String command;

    public WorkerThread(String string) {
        this.command = string;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " Start.");
        processCommand();
        System.out.println(Thread.currentThread().getName() + " End.");
    }

    private void processCommand() {
        try {
            Thread.sleep(3000);
            System.out.println(command);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return this.command;
    }
}