package be.kdg.java2.m11_virtualthreads;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static be.kdg.java2.m11_virtualthreads.Constants.*;

public class Universe {
    private List<Star> stars = new ArrayList<>();

    public Universe() {
        Random random = new Random();
        for (int i = 0; i < STAR_COUNT; i++) {
            Vector position = new Vector();
            double d;
            do {
                position.x = random.nextInt(0, WIDTH);
                position.y = random.nextInt(0, HEIGHT);
                position.z = random.nextInt(0, DEPTH);
                d = Math.sqrt(Math.pow(position.x - WIDTH / 2, 2) + Math.pow(position.y - HEIGHT / 2, 2)
                        + Math.pow(position.z - DEPTH / 2, 2));
            } while (d > MAX_DISTANCE_TO_CENTER);
            Vector velocity = new Vector();
            velocity.x = random.nextDouble(-MAX_SPEED, MAX_SPEED);
            velocity.y = random.nextDouble(-MAX_SPEED, MAX_SPEED);
            velocity.z = random.nextDouble(-MAX_SPEED, MAX_SPEED);
            double mass = random.nextDouble(1, MAX_MASS);
            Star star = new Star(position, velocity, mass);
            star.setUniverse(this);
            stars.add(star);
        }
        //Add a black hole left upper corner
        Star blackhole = new Star(new Vector(WIDTH / 5, HEIGHT / 5, DEPTH / 2),
                new Vector(), 1000000);
        stars.add(blackhole);
    }

    public List<Star> getBalls() {
        return stars;
    }

    //Calculate the gravitational field vector in position
    public Vector getGravitationalAcceleration(Vector position) {
        Vector a = new Vector();
        stars.forEach(b -> {
            double d3 = Math.pow(Math.sqrt(Math.pow(b.getPosition().x - position.x, 2)
                    + Math.pow(b.getPosition().y - position.y, 2)
                    + Math.pow(b.getPosition().z - position.z, 2)), 3);
            if (d3 > 0) {
                a.x += (GRAV_CONSTANT * b.getM() * (b.getPosition().x - position.x)) / d3;
                a.y += (GRAV_CONSTANT * b.getM() * (b.getPosition().y - position.y)) / d3;
                a.z += (GRAV_CONSTANT * b.getM() * (b.getPosition().z - position.z)) / d3;
            }
        });
        return a;
    }
}
