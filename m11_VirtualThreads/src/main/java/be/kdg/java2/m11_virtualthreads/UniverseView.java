package be.kdg.java2.m11_virtualthreads;

import javafx.application.Platform;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;

import static be.kdg.java2.m11_virtualthreads.Constants.*;

public class UniverseView extends BorderPane {
    private Universe universe;
    private GraphicsContext gc;

    public UniverseView(Universe universe) {
        this.universe = universe;
        setStyle("-fx-background-color: black");
        Canvas canvas = new Canvas(WIDTH, HEIGHT);
        this.setCenter(canvas);
        gc = canvas.getGraphicsContext2D();
    }

    public void update() {
        Platform.runLater(() -> {
            gc.setFill(Color.BLACK);
            gc.fillRect(0, 0, WIDTH, HEIGHT);
            universe.getBalls().stream().filter(b->b.getPosition().z>0).forEach(ball -> {
                gc.setFill(ball.getColor());
                double diameter = STAR_DIAMETER /(SCREEN_DISTANCE + ball.getPosition().z);
                gc.fillOval(ball.getPosition().x, ball.getPosition().y, diameter, diameter);
            });
        });
    }
}
