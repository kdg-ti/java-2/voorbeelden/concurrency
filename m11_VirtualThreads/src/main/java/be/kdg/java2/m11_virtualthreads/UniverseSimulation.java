package be.kdg.java2.m11_virtualthreads;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

import static be.kdg.java2.m11_virtualthreads.Constants.*;

public class UniverseSimulation extends Application {
    private Universe universe;
    private UniverseView universeView;

    @Override
    public void start(Stage stage) throws IOException {
        universe = new Universe();
        universeView = new UniverseView(universe);
        Scene scene = new Scene(universeView, WIDTH, HEIGHT);
        stage.setTitle("Universe Simulation");
        stage.setScene(scene);
        stage.setOnCloseRequest(windowEvent -> System.exit(0));
        stage.show();
        universeView.update();//draw the stars once
        universeView.setOnMouseClicked(mouseEvent -> startSimulation());
    }

    public void startSimulation(){
        startUIRefreshLoop();
        //startUpdateLoopWithNormalThreads();
        startUpdateLoopWithVirtualThreads();
    }

    //Startup a VirtualThread for each star!
    private void startUpdateLoopWithVirtualThreads() {
        universe.getBalls().forEach(ball -> {
            Thread.ofVirtual().start(() -> {
                while (true) {
                    ball.move();
                    try {
                        Thread.sleep(UNIVERSE_UPDATE_DELAY);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        });
    }

    //Startup a normal Thread for each star!
    private void startUpdateLoopWithNormalThreads() {
        universe.getBalls().forEach(ball -> {
            Thread t = new Thread(() -> {
                while (true) {
                    ball.move();
                    try {
                        Thread.sleep(UNIVERSE_UPDATE_DELAY);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
            t.start();
        });
    }

    //We run 1 separate Thread for the updating of the UI
    private void startUIRefreshLoop() {
        Thread thread = new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(UI_REFRESH_DELAY);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                universeView.update();
            }
        });
        thread.start();
    }

    public static void main(String[] args) {
        launch();
    }
}