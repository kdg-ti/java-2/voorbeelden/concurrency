package be.kdg.java2.m11_virtualthreads;

public class Vector {
    double x,y,z;

    public Vector() {
    }

    public Vector(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public String toString() {
        return String.format("(%.1f,%.1f,%.1f)", x,y,z);
    }
}
