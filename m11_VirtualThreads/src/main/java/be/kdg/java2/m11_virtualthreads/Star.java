package be.kdg.java2.m11_virtualthreads;

import javafx.scene.paint.Color;

public class Star {
    private Vector position;
    private Vector velocity;
    private double m;
    private Universe universe;

    public Star(Vector position, Vector velocity, double m) {
        this.position = position;
        this.velocity = velocity;
        this.m = m;
    }

    public void setUniverse(Universe universe) {
        this.universe = universe;
    }

    public Vector getPosition() {
        return position;
    }

    public void move() {
        position.x += velocity.x;
        position.y += velocity.y;
        position.z += velocity.z;
        if (this.universe != null) {
            Vector ag = universe.getGravitationalAcceleration(this.position);
            velocity.x += ag.x;
            velocity.y += ag.y;
            velocity.z += ag.z;
        }
    }

    public double getM() {
        return m;
    }

    public Color getColor(){
        return Color.WHITE;
    }
}
