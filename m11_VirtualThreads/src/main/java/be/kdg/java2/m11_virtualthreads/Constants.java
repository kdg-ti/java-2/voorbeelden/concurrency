package be.kdg.java2.m11_virtualthreads;

public class Constants {
    public static final int WIDTH = 800;
    public static final int HEIGHT = 800;
    public static final int DEPTH = 800;
    public static final int STAR_COUNT = 10000;
    public static final int MAX_MASS = 100;
    public static final int MAX_SPEED = 1;
    public static final double GRAV_CONSTANT = 0.1;
    public static final int UI_REFRESH_DELAY = 20;
    public static final int UNIVERSE_UPDATE_DELAY = 10;
    public static final double SCREEN_DISTANCE = 80;
    public static final double STAR_DIAMETER = 1000;
    public static final double MAX_DISTANCE_TO_CENTER = 300;
}
