module be.kdg.java2.m11_virtualthreads {
    requires javafx.controls;
    requires javafx.fxml;


    opens be.kdg.java2.m11_virtualthreads to javafx.fxml;
    exports be.kdg.java2.m11_virtualthreads;
}