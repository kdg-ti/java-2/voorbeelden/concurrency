package be.kdg.deadlock;

/**
 * https://www.baeldung.com/java-dining-philoshophers
 */
public class Philosopher implements Runnable {
    // The forks on either side of this Philosopher
    private final Fork firstFork;
    private final Fork secondFork;

    public Philosopher(Fork firstFork, Fork secondFork) {
        this.firstFork = firstFork;
        this.secondFork = secondFork;
    }

    private void doAction(String action) throws InterruptedException {
        report(action);
        Thread.sleep((int) (Math.random() * 100));
    }

    private void report(String action) {
        System.out.printf("%d : %s: %s\n",
          System.nanoTime(),
          Thread.currentThread().getName(),
          action);
    }

    @Override
    public void run() {
        try {
            while (true) {
                // thinking
                doAction("Thinking");
                synchronized (firstFork) {
                    report("Picked up first fork");
                    synchronized (secondFork) {
                        // eating
                        doAction("Picked up second fork - eating");
                        report("Put down second fork");
                    }
                    // Back to thinking
                    report("Put down first fork");
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}