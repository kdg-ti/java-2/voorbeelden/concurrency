package be.kdg.deadlock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * De klasse Fork maakt gebruik van ReentrantLock
 */
public class Fork {
    public Lock lock;

    public Fork() {
        this.lock = new ReentrantLock();
    }
}
