package be.kdg.concurrency;

import java.util.concurrent.ArrayBlockingQueue;

public class DemoArrayBlockingQueue {
    private static final int QUEUE_SIZE = 10;

    public static void main(String[] args) {
        String[] strArray = {"Alfa", "Bravo", "Charlie", "Delta", "Echo", "Foxtrot", "Golf", "Hotel", "India", "Juliett", "Kilo", "Lima", "Mike", "November", "Oscar", "Papa", "Quebec", "Romeo", "Sierra", "Tango", "Uniform", "Victor", "Whiskey", "X-ray", "Yankee", "Zulu"};
        ArrayBlockingQueue<String> myQueue = new ArrayBlockingQueue<>(QUEUE_SIZE);

        Thread producer = new Thread(() -> {
            for (String string : strArray) {
                myQueue.add(string);
                System.out.format("Toegevoegd: %s - Size: %2d\n", string, myQueue.size());
            }
        });

        Thread consumer = new Thread(() -> {
            for (int i = 0; i < strArray.length; i++) {
                try {
                    String value = myQueue.remove();
                    System.out.format("\t\t\t\t\t\t\t\tVerwijderd: %s - Size: %2d\n", value, myQueue.size());
                    Thread.sleep(100);
                } catch (InterruptedException ex) {  // empty
                }
            }
        });

        producer.start();
        consumer.start();
    }
}
