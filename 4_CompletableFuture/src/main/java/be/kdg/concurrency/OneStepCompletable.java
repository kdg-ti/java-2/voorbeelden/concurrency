package be.kdg.concurrency;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OneStepCompletable {
    private static final String[] wordArray = "May the force be with you".split(" ");

    public static void main(String[] args) throws Exception {
        // zet wordArray om naar woorden stream in hoofdletters
        CompletableFuture<Stream<String>> upperFuture = CompletableFuture.supplyAsync(
                () -> Arrays.stream(wordArray).map(word -> word.toUpperCase()));
        System.out.printf("Thread %s werkt verder tijdens omzetten.\n",
                Thread.currentThread().getName());
        String result = upperFuture.get().collect(Collectors.joining(" "));
        System.out.printf("Resultaat ontvangen in %s: %s\n",
                Thread.currentThread().getName(), result);
        System.out.printf("Einde thread %s.\n", Thread.currentThread().getName());
    }
}



