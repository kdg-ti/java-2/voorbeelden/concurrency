package be.kdg.concurrency;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class ThenAcceptCompletable {
    private static final String[] wordArray = "May the force be with you".split(" ");

    public static void main(String[] args) {
        ExecutorService pool = Executors.newFixedThreadPool(3);
        // zet wordArray om naar woorden stream in hoofdletters:
        CompletableFuture.supplyAsync(() -> Arrays.stream(wordArray)
                        .map(word -> word.toUpperCase())
                , pool)
                .thenAccept(result -> System.out.println("Omzetten words in hoofdletters is gedaan: "
                        + result.collect(Collectors.joining(" "))));
        System.out.println("Thread main werkt verder tijdens omzetten");
        System.out.println("Einde main thread.");
        pool.shutdown();
    }
}


