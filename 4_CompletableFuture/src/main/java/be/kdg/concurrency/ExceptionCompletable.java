package be.kdg.concurrency;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class ExceptionCompletable {
    private static final String[] wordArray = "May the force be with you".split(" ");

    public static void main(String[] args) throws Exception {
        ExecutorService pool = Executors.newFixedThreadPool(3);
        // zet wordArray om naar woorden stream in hoofdletters
        CompletableFuture.supplyAsync(() -> Arrays.stream(wordArray).map(word
                        -> word.toUpperCase()),
                pool)
                .thenApply(stream -> stream.map(word -> new StringBuffer(word).reverse()))
                .handle((result, x) -> x == null
                        ? result.collect(Collectors.joining(" "))
                        : "Foutje: " + x.toString())
                .thenAccept(print -> System.out.println("=> " + print));
        System.out.println("Thread main werkt verder tijdens omzetten");
        System.out.println("Einde main thread.");
        pool.shutdown();
    }
}

